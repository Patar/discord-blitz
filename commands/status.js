module.exports = (self) => {
  self.registerCommand('status', function (msg, args) {
    this.self.editStatus(args.join(''));
    this.send(msg, `Changed status to ${args}`);
  });
};
