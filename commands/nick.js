module.exports = (self) => {
  self.registerCommand('nick', function (msg, args) {
    this.self.editNickname(msg.channel.guild.id, args ? args.join(' ') : null).then(() => this.send(msg, '👌'));
  }, {
    perms: ['changeNickname'],
    noPms: true,
    aliases: ['nickname']
  });
};
